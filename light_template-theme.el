;;; light_template-theme.el --- A template theme with garish colors

;; Copyright (C) 2018 Marcel Brown

;; Author: Marcel Brown
;; Keywords: themes
;; URL: https://gitlab.com/lecramed/emacs_themes/light_template-theme
;; Version: 2018
;; X-Original-Version: 0.1
;; Package-Requires: ((emacs "26.1"))

;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; Commentary:

;; This is a 16 colour template theme with garish colours designed so that when
;; you preview it, it is obvious which settings are being applied to which item.
;; You can then modify the colours defined at the top to suit your preferences.

;; It follows a solarized-ish naming convention so that if you pick your
;; colours accordingly you can perform a palette swap as described here:
;; https://ethanschoonover.com/solarized/

;; To use it, put the following in your Emacs configuration file:
;;
;;   (load-theme 'light_template t)
;;
;; Requirements: Emacs 26.

;;; Credits:

;; Thanks to Paulik Christoph whose material theme was used as a base for the
;; various mode bits: https://github.com/cpaulik/emacs-material-theme

;;; Code:

(deftheme light_template
  "A garish starter theme for Emacs")
(let ((class '((class color) (min-colors 89)))
      ;; palette colours - should roughly follow the dark/light xresources pattern
      ;; https://wiki.archlinux.org/index.php/Color_output_in_console#X_window_system
      (background-l "#191919") ;; (color0) dark theme background
      (background-l-highlight "#383838") ;; (color8) dark theme background highlights
      (foreground-l "#cccccc") ;; (color7) dark theme foreground
      (foreground-l-highlight "#eeeeee") ;; (color15) dark theme foreground highlights, light theme comments
      (accent-1 "#c41313") ;; (color2) red
      (accent-2 "#ff0ae2") ;; (color10) magenta
      (accent-3 "#ce8200") ;; (color3) orange
      (accent-4 "#f2e71a") ;; (color11) yellow
      (accent-5 "#2cad13") ;; (color4) green
      (accent-6 "#09efe8") ;; (color12) cyan
      (accent-7 "#3d4fed") ;; (color5) blue
      (accent-8 "#ae00ff") ;; (color13) purple
      (foreground-d "#693772")  ;; (color6) light theme foreground
      (foreground-d-highlight "#83448e") ;; (color14) light theme foreground highlights, dark theme comments
      (background-d "#d79ee2") ;; (color1) light theme background
      (background-d-highlight "#e6a9f2")) ;; (color9) light theme background highlights

  ;; SET FACES
  (custom-theme-set-faces
   'light_template
   `(default ((,class (:foreground ,foreground-d :background ,background-d))))
   `(bold ((,class (:weight bold))))
   `(bold-italic ((,class (:slant italic :weight bold))))
   `(underline ((,class (:underline t))))
   `(italic ((,class (:slant italic))))

   ;; Font lock
   `(font-lock-builtin-face ((,class (:foreground ,accent-6))))
   `(font-lock-comment-delimiter-face ((,class (:foreground ,foreground-l))))
   `(font-lock-comment-face ((,class (:foreground ,foreground-l-highlight))))
   `(font-lock-constant-face ((,class (:foreground ,accent-5))))
   `(font-lock-doc-face ((,class (:foreground ,foreground-l-highlight))))
   `(font-lock-doc-string-face ((,class (:foreground ,foreground-l-highlight))))
   `(font-lock-function-name-face ((,class (:foreground ,accent-2 :weight bold))))
   `(font-lock-keyword-face ((,class (:foreground ,accent-4))))
   `(font-lock-negation-char-face ((,class (:foreground ,accent-7))))
   `(font-lock-preprocessor-face ((,class (:foreground ,accent-8))))
   `(font-lock-regexp-grouping-backslash ((,class (:foreground ,accent-4))))
   `(font-lock-regexp-grouping-construct ((,class (:foreground ,accent-8))))
   `(font-lock-string-face ((,class (:foreground ,accent-7))))
   `(font-lock-type-face ((,class (:foreground ,accent-3))))
   `(font-lock-variable-name-face ((,class (:foreground ,accent-8 :slant italic))))
   `(font-lock-warning-face ((,class (:foreground ,accent-1 :weight bold))))

   `(highlight-numbers-number ((,class (:foreground ,accent-5))))
   `(shadow ((,class (:foreground ,foreground-l-highlight))))

   ;; Escape and prompt faces
   `(minibuffer-prompt ((,class (:foreground ,accent-6))))
   `(success ((,class (:foreground ,accent-5))))
   `(error ((,class (:foreground ,accent-1))))
   `(warning ((,class (:foreground ,accent-3))))

   ;; Emacs interface
   ;`(linum ((,class (:background ,background-d-highlight :foreground ,foreground-d))))
   ;`(linum-highlight-face ((,class (:background ,background-d-highlight :foreground ,foreground-d-highlight))))
   `(border ((,class (:background ,background-d-highlight))))
   `(vertical-border ((,class (:background ,background-l :foreground, background-l))))
   `(border-glyph ((,class (nil))))
   `(gui-element ((,class (:background ,background-d-highlight :foreground ,foreground-d))))
   `(header-line ((,class (:inherit mode-line :foreground ,accent-7 :background nil :box nil))))
   `(link ((,class (:foreground ,accent-2 :underline t))))
   `(widget-button ((,class (:underline t :weight bold))))
   `(widget-field ((,class (:background ,background-d-highlight :box (:line-width 1 :color ,foreground-d)))))
   ;; Mode line faces
   `(mode-line ((,class (:background ,background-l
				     :foreground ,foreground-l
				     :box (:line-width 2 :color ,accent-7)))))
   `(mode-line-buffer-id ((,class (:background ,background-d-highlight
					       :foreground ,accent-7
					       :slant italic))))
   `(mode-line-inactive ((,class (:inherit mode-line
                                           :background ,background-l-highlight
                                           :foreground ,foreground-l-highlight
					   :weight normal
                                           :box (:line-width 2 :color ,background-d)))))
   `(mode-line-emphasis ((,class (:foreground ,foreground-d :slant italic))))
   `(mode-line-highlight ((,class (:foreground ,accent-8 :box nil))))
   `(minibuffer-prompt ((,class (:foreground ,accent-7))))
   ;; Highlight faces
   `(cursor ((,class (:background ,accent-3))))
   `(hl-line ((,class (:inverse-video nil :background ,background-d-highlight))))
   `(fringe ((,class (:background ,background-d))))
   `(highlight ((,class (:inverse-video nil :background ,background-d-highlight))))
   `(region ((,class (:background ,background-l))))
   `(secondary-selection ((,class (:background ,background-l-highlight))))
   ;; whitespace
   `(trailing-whitespace ((,class (:foreground ,accent-1 :inverse-video t :underline nil))))
   `(whitespace-trailing ((,class (:foreground ,accent-1 :inverse-video t :underline nil))))
   `(whitespace-space-after-tab ((,class (:foreground ,accent-1 :inverse-video t :underline nil))))
   `(whitespace-space-before-tab ((,class (:foreground ,accent-1 :inverse-video t :underline nil))))
   `(whitespace-empty ((,class (:foreground ,accent-1 :inverse-video t :underline nil))))
   `(whitespace-line ((,class (:background nil :foreground ,accent-1))))
   `(whitespace-indentation ((,class (:background nil :foreground ,accent-6))))
   `(whitespace-space ((,class (:background nil :foreground ,accent-2))))
   `(whitespace-newline ((,class (:background nil :foreground ,accent-2))))
   `(whitespace-tab ((,class (:background nil :foreground ,accent-2))))
   `(whitespace-hspace ((,class (:background nil :foreground ,accent-2))))

   ;; avy-jump-mode
   `(avy-background-face ((,class (:foreground ,foreground-l
                                   :weight normal))))
   `(avy-lead-face-0 ((,class (:foreground ,foreground-d
                               :background ,background-d
                                        :weight bold))))
   `(avy-lead-face ((,class (:foreground ,foreground-d
                             :background ,background-d
                             :weight bold))))
   ;; Flyspell faces
   `(flyspell-duplicate ((,class (:underline ,accent-3))))
   `(flyspell-incorrect ((,class (:underline ,accent-1))))

   ;; Flycheck
   `(flycheck-error ((,class (:underline (:style wave :color ,accent-1)))))
   `(flycheck-warning ((,class (:underline (:style wave :color ,accent-3)))))

   ;; Highlight indentation
   `(highlight-indentation-face ((,class (:background, background-d-highlight))))
   `(highlight-indentation-current-column-face ((,class (:background, foreground-d-highlight))))

   ;; Flymake
   `(flymake-warnline ((,class (:underline (:style wave :color ,accent-3) :background ,background-d))))
   `(flymake-errline ((,class (:underline (:style wave :color ,accent-1) :background ,background-d))))

   ;; Rainbow-delimiters
   `(rainbow-delimiters-depth-1-face ((,class (:foreground ,"#e91e63"))))
   `(rainbow-delimiters-depth-2-face ((,class (:foreground ,"#2196F3"))))
   `(rainbow-delimiters-depth-3-face ((,class (:foreground ,"#EF6C00"))))
   `(rainbow-delimiters-depth-4-face ((,class (:foreground ,"#B388FF"))))
   `(rainbow-delimiters-depth-5-face ((,class (:foreground ,"#76ff03"))))
   `(rainbow-delimiters-depth-6-face ((,class (:foreground ,"#26A69A"))))
   `(rainbow-delimiters-depth-7-face ((,class (:foreground ,"#FFCDD2"))))
   `(rainbow-delimiters-depth-8-face ((,class (:foreground ,"#795548"))))
   `(rainbow-delimiters-depth-9-face ((,class (:foreground ,"#DCE775"))))
   `(rainbow-delimiters-unmatched-face ((,class (:foreground ,foreground-d :background ,"#EF6C00"))))

   ;; Parenthesis matching (built-in)
   `(show-paren-match-face ((,class (:background ,accent-6 :foreground "black"))))
   `(show-paren-mismatch-face ((,class (:background ,accent-1 :foreground "white"))))

   ;; Search
   `(match ((,class (:foreground ,background-d :background ,accent-5 :inverse-video nil))))
   `(isearch ((,class (:foreground ,foreground-d :background ,accent-5))))
   `(isearch-lazy-highlight-face ((,class (:foreground ,background-d :background ,accent-5 :inverse-video nil))))
   `(lazy-highlight-face ((,class (:foreground ,background-d :background ,accent-5 :inverse-video nil))))
   `(isearch-fail ((,class (:background ,background-d :inherit font-lock-warning-face :inverse-video t))))

   ;; which-function
   `(which-func ((,class (:foreground ,accent-7 :background nil))))

   ;; Diffs
   `(diff-hl-insert ((,class (:background ,accent-5 :foreground ,accent-5))))
   `(diff-hl-change ((,class (:background ,accent-6 :foreground ,accent-6))))
   `(diff-hl-delete ((,class (:background ,accent-3 :foreground ,accent-3))))

   `(diff-added ((,class (:foreground ,accent-5))))
   `(diff-changed ((,class (:foreground ,accent-6))))
   `(diff-removed ((,class (:foreground ,accent-3))))
   `(diff-header ((,class (:foreground ,accent-6 :background nil))))
   `(diff-file-header ((,class (:foreground ,accent-7 :background nil))))
   `(diff-hunk-header ((,class (:foreground ,accent-8))))
   `(diff-refine-added ((,class (:inherit diff-added :inverse-video t))))
   `(diff-refine-removed ((,class (:inherit diff-removed :inverse-video t))))

   `(ediff-even-diff-A ((,class (:foreground nil :background nil :inverse-video t))))
   `(ediff-even-diff-B ((,class (:foreground nil :background nil :inverse-video t))))
   `(ediff-odd-diff-A  ((,class (:foreground ,foreground-l-highlight :background nil :inverse-video t))))
   `(ediff-odd-diff-B  ((,class (:foreground ,foreground-l-highlight :background nil :inverse-video t))))

   ;; Magit
   `(magit-branch ((,class (:foreground ,accent-5))))
   `(magit-diff-added ((,class (:inherit diff-added))))
   `(magit-diff-added-highlight ((,class (:inherit magit-diff-added
                                          :background ,foreground-d-highlight))))
   `(magit-diff-removed ((,class (:inherit diff-removed))))
   `(magit-diff-removed-highlight ((,class (:inherit magit-diff-removed
                                            :background ,foreground-d-highlight))))
   `(magit-header ((,class (:inherit nil :weight bold))))
   `(magit-item-highlight ((,class (:inherit highlight :background nil))))
   `(magit-log-author ((,class (:foreground ,accent-6))))
   `(magit-log-graph ((,class (:foreground ,foreground-l-highlight))))
   `(magit-log-date ((,class (:foreground ,accent-4))))
   `(magit-section-title ((,class (:foreground ,accent-7 :weight bold))))
   `(magit-section-highlight           ((t (:background ,background-d-highlight))))
   `(magit-section-heading             ((t (:foreground ,accent-4 :weight bold))))
   `(magit-diff-file-heading           ((t (:weight bold))))
   `(magit-diff-file-heading-highlight ((t (:background ,background-d-highlight  :weight bold))))
   `(magit-diff-file-heading-selection ((t (:background ,background-d
                                            :foreground ,accent-3 :weight bold))))
   `(magit-diff-hunk-heading           ((t (:background ,accent-2))))
   `(magit-diff-hunk-heading-highlight ((t (:background ,accent-2))))
   `(magit-diff-hunk-heading-selection ((t (:background ,background-d
                                            :foreground ,accent-3))))
   `(magit-diff-lines-heading          ((t (:background ,accent-3
                                            :foreground ,background-d))))
   `(magit-blame-heading          ((t (:background ,foreground-d-highlight
                                       :foreground ,accent-6))))
   `(magit-blame-date             ((t (:background ,foreground-d-highlight
                                                   :foreground ,accent-5))))
   `(magit-blame-summary          ((t (:background ,foreground-d-highlight
                                       :foreground ,accent-5))))
   `(magit-diff-context-highlight      ((t (:background ,foreground-d-highlight
                                            :foreground ,accent-6))))
   `(magit-diffstat-added   ((t (:foreground ,accent-5))))
   `(magit-diffstat-removed ((t (:foreground ,accent-1))))
   `(magit-process-ok    ((t (:foreground ,accent-5  :weight bold))))
   `(magit-process-ng    ((t (:foreground ,accent-1    :weight bold))))
   `(magit-branch-local  ((t (:foreground ,accent-7   :weight bold))))
   `(magit-branch-remote ((t (:foreground ,accent-5  :weight bold))))
   `(magit-tag           ((t (:foreground ,accent-3 :weight bold))))
   `(magit-hash          ((t (:foreground ,foreground-l-highlight))))
   `(magit-sequence-stop ((t (:foreground ,accent-5))))
   `(magit-sequence-part ((t (:foreground ,accent-4))))
   `(magit-sequence-head ((t (:foreground ,accent-7))))
   `(magit-sequence-drop ((t (:foreground ,accent-1))))

   ;; git-gutter
   `(git-gutter:modified ((,class (:foreground ,accent-8 :weight bold))))
   `(git-gutter:added ((,class (:foreground ,accent-5 :weight bold))))
   `(git-gutter:deleted ((,class (:foreground ,accent-1 :weight bold))))
   `(git-gutter:unchanged ((,class (:background ,accent-4))))
   ;; git-gutter-fringe
   `(git-gutter-fr:modified ((,class (:foreground ,accent-8 :weight bold))))
   `(git-gutter-fr:added ((,class (:foreground ,accent-5 :weight bold))))
   `(git-gutter-fr:deleted ((,class (:foreground ,accent-1 :weight bold))))

   ;; Compilation (most faces politely inherit from 'success, 'error, 'warning etc.)
   `(compilation-column-number ((,class (:foreground ,accent-4))))
   `(compilation-line-number ((,class (:foreground ,accent-4))))
   `(compilation-message-face ((,class (:foreground ,accent-7))))
   `(compilation-mode-line-exit ((,class (:foreground ,accent-5))))
   `(compilation-mode-line-fail ((,class (:foreground ,accent-1))))
   `(compilation-mode-line-run ((,class (:foreground ,accent-7))))

   ;; Grep
   `(grep-context-face ((,class (:foreground ,foreground-l-highlight))))
   `(grep-error-face ((,class (:foreground ,accent-1 :weight bold :underline t))))
   `(grep-hit-face ((,class (:foreground ,accent-7))))
   `(grep-match-face ((,class (:foreground nil :background nil :inherit match))))

   `(regex-tool-matched-face ((,class (:foreground nil :background nil :inherit match))))

   ;; Helm
   `(helm-header ((,class (:foreground ,foreground-d :background ,background-d))))
   `(helm-match ((,class (:foreground ,accent-3))))
   `(helm-selection ((,class (:background ,background-d-highlight))))
   `(helm-ff-file ((,class (:foreground ,foreground-d ))))
   `(helm-ff-directory ((,class (:foreground ,accent-6 ))))
   `(helm-ff-executable ((,class (:foreground ,accent-5 ))))
   `(helm-buffer-directory ((,class (:foreground ,accent-6))))
   `(helm-buffer-file ((,class (:foreground ,foreground-d))))
   `(helm-grep-file ((,class (:foreground ,accent-6 :underline t))))
   `(helm-buffer-process ((,class (:foreground ,accent-1))))
   `(helm-buffer-not-saved ((,class (:foreground ,accent-3))))
   `(helm-candidate-number ((,class (:foreground ,foreground-d :background ,background-d-highlight))))
   `(helm-source-header ((,class (:background ,accent-2 :foreground ,foreground-d :height 1.3 :bold t ))))

   ;; Outline
   `(outline-1 ((,class (:inherit nil :foreground ,accent-1))))
   `(outline-2 ((,class (:inherit nil :foreground ,accent-2))))
   `(outline-3 ((,class (:inherit nil :foreground ,accent-3))))
   `(outline-4 ((,class (:inherit nil :foreground ,accent-4))))
   `(outline-5 ((,class (:inherit nil :foreground ,accent-5))))
   `(outline-6 ((,class (:inherit nil :foreground ,accent-6))))
   `(outline-7 ((,class (:inherit nil :foreground ,accent-7))))
   `(outline-8 ((,class (:inherit nil :foreground ,accent-8))))
   `(outline-9 ((,class (:inherit nil :foreground ,foreground-d))))

   ;; Org
   `(org-agenda-structure ((,class (:foreground ,accent-6 :bold t))))
   `(org-agenda-date ((,class (:foreground ,accent-7 :underline nil))))
   `(org-agenda-done ((,class (:foreground ,accent-5))))
   `(org-agenda-dimmed-todo-face ((,class (:foreground ,foreground-l-highlight))))
   `(org-block ((,class (:foreground ,accent-5 :background ,background-d))))
   `(org-block-background ((,t (:background ,background-d-highlight))))
   `(org-code ((,class (:foreground ,accent-5 :background ,background-d-highlight))))
   `(org-column ((,class (:background ,background-d-highlight))))
   `(org-column-title ((,class (:inherit org-column :weight bold :underline t))))
   `(org-date ((,class (:foreground ,accent-2 :underline t))))
   `(org-document-info ((,class (:foreground ,accent-6 :height 1.35))))
   `(org-document-info-keyword ((,class (:foreground ,accent-5 :height 1.35))))
   `(org-document-title ((,class (:weight bold :foreground ,accent-2 :height 1.35))))
   `(org-ellipsis ((,class (:foreground ,foreground-l-highlight))))
   `(org-footnote ((,class (:foreground ,accent-2))))
   `(org-formula ((,class (:foreground ,accent-1))))
   `(org-hide ((,class (:foreground ,background-d :background ,background-d))))
   `(org-link ((,class (:inherit link))))
   `(org-scheduled ((,class (:foreground ,accent-5))))
   `(org-scheduled-previously ((,class (:foreground ,accent-3))))
   `(org-scheduled-today ((,class (:foreground ,accent-5))))
   `(org-special-keyword ((,class (:foreground ,foreground-l-highlight))))
   `(org-table ((,class (:foreground ,accent-5 :background ,background-d))))
   `(org-upcoming-deadline ((,class (:foreground ,accent-3))))
   `(org-warning ((,class (:weight bold :foreground ,accent-1))))
   `(org-block-begin-line ((,class (:foreground ,background-l-highlight :background ,foreground-l-highlight
                                    :box (:line-width -1 :color ,foreground-l)))))
   `(org-block-end-line ((,class (:foreground ,background-l :background ,foreground-l
                                  :box (:line-width -1 :color ,foreground-l-highlight)))))
   `(org-kbd ((,class (:background ,foreground-l :foreground ,foreground-d
                       :box (:line-width 1 :color nil :style pressed-button)))))

   ;; handle todo faces in init file
   ;`(org-todo ((,class (:foreground ,accent-3 :bold t :background ,accent-2))))
   ;`(org-done ((,class (:foreground ,accent-5 :bold t :background ,accent-6))))

   `(org-level-1 ((,class (:background ,accent-1
			   :foreground ,background-l
                           :weight bold
                           ;:box (:line-width 2 :color ,background-d-highlight)
                           :height 1.125))))
   `(org-level-2 ((,class (:background ,accent-2
			   :foreground ,background-l
                           ;:box (:line-width -1 :color ,background-d-highlight)
                           :height 1.125))))
   `(org-level-3 ((,class (:background ,accent-3
			   :foreground ,background-l
		       	   :height 1.125))))
   `(org-level-4 ((,class (:inherit outline-4 :height 1.1))))
   `(org-level-5 ((,class (:inherit outline-5 ))))
   `(org-level-6 ((,class (:inherit outline-6 ))))
   `(org-level-7 ((,class (:inherit outline-7 ))))
   `(org-level-8 ((,class (:inherit outline-8 ))))
   `(org-level-9 ((,class (:inherit outline-9 ))))

   ;; Markdown
   `(markdown-header-face-1 ((,class (:inherit outline-1 :weight bold :height 1.3))))
   `(markdown-header-face-2 ((,class (:inherit outline-2 :weight bold :height 1.2))))
   `(markdown-header-face-3 ((,class (:inherit outline-3 :weight bold :height 1.1))))
   `(markdown-header-face-4 ((,class (:inherit outline-4 :weight bold))))
   `(markdown-header-face-5 ((,class (:inherit outline-5))))
   `(markdown-header-face-6 ((,class (:inherit outline-6))))
   `(markdown-header-face-7 ((,class (:inherit outline-7))))
   `(markdown-header-face-8 ((,class (:inherit outline-8))))
   `(markdown-header-face-9 ((,class (:inherit outline-9))))
   `(markdown-header-delimiter-face ((,class (:foreground ,foreground-d
                                              :weight bold
                                              :height 1.1))))
   `(markdown-url-face ((,class (:inherit link))))
   `(markdown-link-face ((,class (:foreground ,accent-7 :underline t))))

   ;; nxml
   `(nxml-name-face ((,class (:foreground unspecified :inherit font-lock-constant-face))))
   `(nxml-attribute-local-name-face ((,class (:foreground unspecified :inherit font-lock-variable-name-face))))
   `(nxml-ref-face ((,class (:foreground unspecified :inherit font-lock-preprocessor-face))))
   `(nxml-delimiter-face ((,class (:foreground unspecified :inherit font-lock-keyword-face))))
   `(nxml-delimited-data-face ((,class (:foreground unspecified :inherit font-lock-string-face))))
   `(rng-error-face ((,class (:underline ,accent-1))))

   ;; Message-mode
   `(message-header-other ((,class (:foreground nil :background nil :weight normal))))
   `(message-header-subject ((,class (:inherit message-header-other :weight bold :foreground ,accent-4))))
   `(message-header-to ((,class (:inherit message-header-other :weight bold :foreground ,accent-3))))
   `(message-header-cc ((,class (:inherit message-header-to :foreground nil))))
   `(message-header-name ((,class (:foreground ,accent-7 :background nil))))
   `(message-header-newsgroups ((,class (:foreground ,accent-6 :background nil :slant normal))))
   `(message-separator ((,class (:foreground ,accent-8))))

   ;; cfw emacs calendar
   `(cfw:face-title ((,class (:background ,background-d :foreground ,foreground-d :height 1.3 :weight bold))))
   `(cfw:face-today ((,class (:foreground ,foreground-d))))
   `(cfw:face-day-title ((,class (:background ,background-d-highlight :foreground ,foreground-d))))
   `(cfw:face-today-title ((,class (:background ,background-l-highlight :foreground ,foreground-d))))
   `(cfw:face-header ((,class (:background ,background-d-highlight :foreground ,foreground-d))))
   `(cfw:face-sunday ((,class (:background ,background-d-highlight :foreground ,accent-6 :weight bold))))
   `(cfw:face-saturday ((,class (:background ,background-d-highlight :foreground ,accent-6 :weight bold))))
   `(cfw:face-select ((,class (:background ,accent-2 :foreground ,foreground-d))))
   `(cfw:face-toolbar ((,class (:background ,accent-6 :foreground ,background-d :weight bold))))
   `(cfw:face-toolbar-button-off ((,class (:background ,accent-6 :foreground ,background-d :weight bold))))
   `(cfw:face-toolbar-button-on ((,class (:background ,accent-6 :foreground ,background-l-highlight :weight bold))))
   `(cfw:face-holiday ((,class (:background ,background-d-highlight :foreground ,accent-5 :weight bold))))

   ;; Ledger-mode
   `(ledger-font-comment-face ((,class (:inherit font-lock-comment-face))))
   `(ledger-font-occur-narrowed-face ((,class (:inherit font-lock-comment-face :invisible t))))
   `(ledger-font-occur-xact-face ((,class (:inherit highlight))))
   `(ledger-font-payee-cleared-face ((,class (:foreground ,accent-5))))
   `(ledger-font-payee-uncleared-face ((,class (:foreground ,accent-6))))
   `(ledger-font-posting-account-cleared-face ((,class (:foreground ,accent-7))))
   `(ledger-font-posting-account-face ((,class (:foreground ,accent-8))))
   `(ledger-font-posting-account-pending-face ((,class (:foreground ,accent-4))))
   `(ledger-font-xact-highlight-face ((,class (:inherit highlight))))
   `(ledger-occur-narrowed-face ((,class (:inherit font-lock-comment-face :invisible t))))
   `(ledger-occur-xact-face ((,class (:inherit highlight))))

   ;; Latex
   `(font-latex-bold-face                 ((t (:inherit bold :foreground ,foreground-d))))
   `(font-latex-doctex-documentation-face ((t (:background unspecified))))
   `(font-latex-doctex-preprocessor-face ((t (:inherit (font-latex-doctex-documentation-face
                                                        font-lock-builtin-face font-lock-preprocessor-face)))))
   `(font-latex-italic-face               ((t (:inherit italic :foreground ,foreground-d))))
   `(font-latex-math-face                 ((t (:foreground ,accent-7))))
   `(font-latex-sectioning-0-face         ((t (:inherit outline-1 :height 1.4))))
   `(font-latex-sectioning-1-face         ((t (:inherit outline-2 :height 1.35))))
   `(font-latex-sectioning-2-face         ((t (:inherit outline-3 :height 1.3))))
   `(font-latex-sectioning-3-face         ((t (:inherit outline-4 :height 1.25))))
   `(font-latex-sectioning-4-face         ((t (:inherit outline-5 :height 1.2))))
   `(font-latex-sectioning-5-face         ((t (:inherit outline-6 :height 1.1))))
   `(font-latex-sedate-face               ((t (:foreground ,accent-5))))
   `(font-latex-slide-title-face          ((t (:inherit font-lock-type-face :weight bold :height 1.2))))
   `(font-latex-string-face               ((t (:inherit font-lock-string-face))))
   `(font-latex-subscript-face            ((t (:height 0.8))))
   `(font-latex-superscript-face          ((t (:height 0.8))))
   `(font-latex-warning-face              ((t (:inherit font-lock-warning-face))))

   ;; stripe-buffer
   `(stripe-highlight ((,class (:background ,background-d-highlight))))
   `(stripe-hl-line ((,class (:background ,accent-2 :foreground ,foreground-d))))

   ;; erc
   `(erc-direct-msg-face ((,class (:foreground ,accent-3))))
   `(erc-error-face ((,class (:foreground ,accent-1))))
   `(erc-header-face ((,class (:foreground ,foreground-d :background ,accent-2))))
   `(erc-input-face ((,class (:foreground ,accent-5))))
   `(erc-keyword-face ((,class (:foreground ,accent-4))))
   `(erc-current-nick-face ((,class (:foreground ,accent-5))))
   `(erc-my-nick-face ((,class (:foreground ,accent-5))))
   `(erc-nick-default-face ((,class (:weight normal :foreground ,accent-8))))
   `(erc-nick-msg-face ((,class (:weight normal :foreground ,accent-4))))
   `(erc-notice-face ((,class (:foreground ,foreground-l-highlight))))
   `(erc-pal-face ((,class (:foreground ,accent-3))))
   `(erc-prompt-face ((,class (:foreground ,accent-7))))
   `(erc-timestamp-face ((,class (:foreground ,accent-6))))
   `(erc-keyword-face ((,class (:foreground ,accent-5))))

   ;; ansi-term
   `(term ((,class (:foreground nil :background nil :inherit default))))
   `(term-color-black   ((,class (:foreground ,foreground-d :background ,foreground-d))))
   `(term-color-red     ((,class (:foreground ,accent-1 :background ,accent-1))))
   `(term-color-green   ((,class (:foreground ,accent-5 :background ,accent-5))))
   `(term-color-yellow  ((,class (:foreground ,accent-4 :background ,accent-4))))
   `(term-color-blue    ((,class (:foreground ,accent-7 :background ,accent-7))))
   `(term-color-magenta ((,class (:foreground ,accent-2 :background ,accent-2))))
   `(term-color-cyan    ((,class (:foreground ,accent-6 :background ,accent-6))))
   `(term-color-white   ((,class (:foreground ,background-d :background ,background-d))))

   ;; Gnus
   ;`(gnus-cite-1 ((,class (:inherit outline-1 :foreground nil))))
   ;`(gnus-cite-2 ((,class (:inherit outline-2 :foreground nil))))
   ;`(gnus-cite-3 ((,class (:inherit outline-3 :foreground nil))))
   ;`(gnus-cite-4 ((,class (:inherit outline-4 :foreground nil))))
   ;`(gnus-cite-5 ((,class (:inherit outline-5 :foreground nil))))
   ;`(gnus-cite-6 ((,class (:inherit outline-6 :foreground nil))))
   ;`(gnus-cite-7 ((,class (:inherit outline-7 :foreground nil))))
   ;`(gnus-cite-8 ((,class (:inherit outline-8 :foreground nil))))
   ;; there are several more -cite- faces...

   ;`(gnus-header-content ((,class (:inherit message-header-other))))
   ;`(gnus-header-subject ((,class (:inherit message-header-subject))))
   ;`(gnus-header-from ((,class (:inherit message-header-other-face :weight bold :foreground ,accent-3))))
   ;`(gnus-header-name ((,class (:inherit message-header-name))))
   ;`(gnus-button ((,class (:inherit link :foreground nil))))
   ;`(gnus-signature ((,class (:inherit font-lock-comment-face))))

   ;`(gnus-summary-normal-unread ((,class (:foreground ,foreground-d :weight bold))))
   ;`(gnus-summary-normal-read ((,class (:foreground ,foreground-l-highlight :weight normal))))
   ;`(gnus-summary-normal-ancient ((,class (:foreground ,accent-6 :weight normal))))
   ;`(gnus-summary-normal-ticked ((,class (:foreground ,accent-3 :weight normal))))
   ;`(gnus-summary-low-unread ((,class (:foreground ,foreground-l-highlight :weight normal))))
   ;`(gnus-summary-low-read ((,class (:foreground ,foreground-l-highlight :weight normal))))
   ;`(gnus-summary-low-ancient ((,class (:foreground ,foreground-l-highlight :weight normal))))
   ;`(gnus-summary-high-unread ((,class (:foreground ,accent-4 :weight normal))))
   ;`(gnus-summary-high-read ((,class (:foreground ,accent-5 :weight normal))))
   ;`(gnus-summary-high-ancient ((,class (:foreground ,accent-5 :weight normal))))
   ;`(gnus-summary-high-ticked ((,class (:foreground ,accent-3 :weight normal))))
   ;`(gnus-summary-cancelled ((,class (:foreground ,accent-1 :background nil :weight normal))))

   ;`(gnus-group-mail-low ((,class (:foreground ,foreground-l-highlight))))
   ;`(gnus-group-mail-low-empty ((,class (:foreground ,foreground-l-highlight))))
   ;`(gnus-group-mail-1 ((,class (:foreground nil :weight normal :inherit outline-1))))
   ;`(gnus-group-mail-2 ((,class (:foreground nil :weight normal :inherit outline-2))))
   ;`(gnus-group-mail-3 ((,class (:foreground nil :weight normal :inherit outline-3))))
   ;`(gnus-group-mail-4 ((,class (:foreground nil :weight normal :inherit outline-4))))
   ;`(gnus-group-mail-5 ((,class (:foreground nil :weight normal :inherit outline-5))))
   ;`(gnus-group-mail-6 ((,class (:foreground nil :weight normal :inherit outline-6))))
   ;`(gnus-group-mail-1-empty ((,class (:inherit gnus-group-mail-1 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-mail-2-empty ((,class (:inherit gnus-group-mail-2 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-mail-3-empty ((,class (:inherit gnus-group-mail-3 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-mail-4-empty ((,class (:inherit gnus-group-mail-4 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-mail-5-empty ((,class (:inherit gnus-group-mail-5 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-mail-6-empty ((,class (:inherit gnus-group-mail-6 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-news-1 ((,class (:foreground nil :weight normal :inherit outline-5))))
   ;`(gnus-group-news-2 ((,class (:foreground nil :weight normal :inherit outline-6))))
   ;`(gnus-group-news-3 ((,class (:foreground nil :weight normal :inherit outline-7))))
   ;`(gnus-group-news-4 ((,class (:foreground nil :weight normal :inherit outline-8))))
   ;`(gnus-group-news-5 ((,class (:foreground nil :weight normal :inherit outline-1))))
   ;`(gnus-group-news-6 ((,class (:foreground nil :weight normal :inherit outline-2))))
   ;`(gnus-group-news-1-empty ((,class (:inherit gnus-group-news-1 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-news-2-empty ((,class (:inherit gnus-group-news-2 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-news-3-empty ((,class (:inherit gnus-group-news-3 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-news-4-empty ((,class (:inherit gnus-group-news-4 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-news-5-empty ((,class (:inherit gnus-group-news-5 :foreground ,foreground-l-highlight))))
   ;`(gnus-group-news-6-empty ((,class (:inherit gnus-group-news-6 :foreground ,foreground-l-highlight))))

   ;; Python-specific overrides
   ;`(py-builtins-face ((,class (:foreground ,"#ff7043" :weight normal))))

   ;; ein (emacs-ipython-notebook) specific colors
   ;`(ein:cell-input-area ((,class (:background ,"#1e2930"))))
   ;`(ein:cell-input-prompt ((,class (:inherit org-block-begin-line))))
   ;`(ein:cell-output-prompt ((,class (:inherit org-block-end-line))))

   ;; Company autocomplete
   ;; `(company-echo ((,class ())))
   ;; `(company-echo-common ((,class ())))
   ;`(company-preview ((,class (:foreground ,foreground-l-highlight :background ,foreground-l))))
   ;`(company-preview-common ((,class (:foreground ,foreground-l-highlight :background ,foreground-l)))) ; same background as highlight-line
   ;; `(company-preview-search ((,class ())))
   ;`(company-scrollbar-bg ((,class (:background ,background-l-highlight))))
   ;`(company-scrollbar-fg ((,class (:background ,background-l))))
   ;`(company-template-field ((,class (:background ,foreground-l))))
   ;`(company-tooltip ((,class (:weight bold :foreground, foreground-d-highlight :background ,foreground-l))))
   ;`(company-tooltip-annotation ((,class (:weight normal :foreground ,foreground-l-highlight :background ,foreground-l))))
   ;`(company-tooltip-annotation-selection ((,class (:weight normal :inherit company-tooltip-selection))))
   ;`(company-tooltip-common ((,class (:weight normal :inherit company-tooltip))))
   ;`(company-tooltip-common-selection ((,class (:weight normal :inherit company-tooltip-selection))))
   ;; `(company-tooltip-mouse ((,class ())))
   ;; `(company-tooltip-search ((,class ())))
   ;`(company-tooltip-selection ((,class (:weight bold :foreground ,foreground-d :background ,background-d-highlight))))

   ;; rpm-spec-mode
   ;`(rpm-spec-dir-face ((,class (:foreground ,accent-5))))
   ;`(rpm-spec-doc-face ((,class (:foreground ,accent-5))))
   ;`(rpm-spec-ghost-face ((,class (:foreground ,accent-1))))
   ;`(rpm-spec-macro-face ((,class (:foreground ,accent-4))))
   ;`(rpm-spec-obsolete-tag-face ((,class (:foreground ,accent-1))))
   ;`(rpm-spec-package-face ((,class (:foreground ,accent-1))))
   ;`(rpm-spec-section-face ((,class (:foreground ,accent-4))))
   ;`(rpm-spec-tag-face ((,class (:foreground ,accent-7))))
   ;`(rpm-spec-var-face ((,class (:foreground ,accent-1))))

   ;; Misc
   `(custom-variable-tag ((,class (:foreground ,accent-7))))
   `(custom-group-tag ((,class (:foreground ,accent-7))))
   `(custom-state ((,class (:foreground ,accent-5))))

   `(sh-heredoc ((,class (:foreground nil :inherit font-lock-string-face :weight normal))))
   `(sh-quoted-exec ((,class (:foreground nil :inherit font-lock-preprocessor-face))))

   `(csv-separator-face ((,class (:foreground ,accent-3))))

   ) ;; closes custom-theme-set-faces

  ;; SET VARIABLES
  (custom-theme-set-variables
   'light_template
   `(fci-rule-color ,background-d-highlight)
   `(vc-annotate-color-map
     '((20  . ,accent-1)
       (40  . ,accent-3)
       (60  . ,accent-4)
       (80  . ,accent-5)
       (100 . ,accent-6)
       (120 . ,accent-7)
       (140 . ,accent-8)
       (160 . ,accent-1)
       (180 . ,accent-3)
       (200 . ,accent-4)
       (220 . ,accent-5)
       (240 . ,accent-6)
       (260 . ,accent-7)
       (280 . ,accent-8)
       (300 . ,accent-1)
       (320 . ,accent-3)
       (340 . ,accent-4)
       (360 . ,accent-5)))
   `(vc-annotate-very-old-color nil)
   `(vc-annotate-background nil)
   
   `(ansi-color-names-vector (vector ,foreground-d ,accent-1 ,accent-5 ,accent-4 ,accent-7 ,accent-8 ,accent-6 ,background-d))
   '(ansi-color-faces-vector [default bold shadow italic underline bold bold-italic bold])

   ) ;; closes custom-theme-set-variables
  ) ;; closes let

;;;###autoload
;(when (and (boundp 'custom-theme-load-path)
;           load-file-name)
  ;; add theme folder to `custom-theme-load-path' when installing over MELPA
;  (add-to-list 'custom-theme-load-path
;               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'light_template)

;;; light_template-theme.el ends here
