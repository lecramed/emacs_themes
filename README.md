# emacs themes
Some nice (?) emacs themes. The template themes are a good base if you want to start 
your own theme because the colours are hard to mistake.

# usage
Put them in your load path, or add their location to your init file like so:
```
;; Custom theme path
(add-to-list 'custom-theme-load-path "~/location/of/your/emacs_themes/")
```

Then add something like this to your init file:
```
(load-theme 'bootleg-solarized-dark t)
```
